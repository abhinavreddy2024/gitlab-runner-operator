package runner

import (
	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	"k8s.io/apimachinery/pkg/types"
)

// RegistrationTokenSecret returns name of secret containing the
// runner-registration-token and runner-token keys
func RegistrationTokenSecret(cr *gitlabv1beta2.Runner) types.NamespacedName {
	return types.NamespacedName{
		Namespace: cr.Namespace,
		Name:      cr.Spec.RegistrationToken,
	}
}
